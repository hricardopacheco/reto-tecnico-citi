package com.citi.reto.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import com.citi.reto.model.Cliente;
import com.citi.reto.service.impl.ClienteImpl;
import com.google.gson.Gson;

@Component
public class ClienteController {

	@Autowired
	private ClienteImpl impl;

	public Cliente saveCliente(RequestEntity<Cliente> inEntity) {
		Cliente cliente = getData(inEntity);
		cliente.setFechaAlta(new Date().toString());
		cliente.setCliente_id(impl.selectAll().size() + 1L);
		cliente.setStatus(true);
		impl.insert(cliente);
		return cliente;
	}

	public Cliente updateCliente(RequestEntity<Cliente> inEntity) {
		Cliente cliente = getData(inEntity);
		Cliente oldData = impl.findById(cliente.getCliente_id()).get();
		cliente.setFechaAlta(oldData.getFechaAlta());
		return impl.update(cliente);
	}
	
	public Cliente deleteCliente(RequestEntity<Cliente> inEntity) {
		Cliente cliente = getData(inEntity);
		Cliente delete = impl.findById(cliente.getCliente_id()).get();
		impl.deleteEntity(cliente.getCliente_id());
		return  delete;
	}
	
	public Cliente findById(RequestEntity<Cliente> inEntity) {
		return impl.findById(getData(inEntity).getCliente_id()).get();
	}
	
	public List<Cliente>findAll() {
		return impl.selectAll();
	}

	private Cliente getData(RequestEntity<Cliente> inEntity) {
		Gson jsonGson = new Gson();
		String json = jsonGson.toJson(inEntity.getBody());
		new Gson();
		return jsonGson.fromJson(json, Cliente.class);
	}
}
