package com.citi.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import com.citi.reto.model.Estado;
import com.citi.reto.service.impl.EstadoImpl;
import com.google.gson.Gson;

@Component
public class EstadoController {

	@Autowired
	private EstadoImpl impl;
	
	public Estado saveEstado(RequestEntity<Estado> inEntity) {
		Estado estado = getData(inEntity);
		estado.setEstado_Id(impl.selectAll().size()+1L);
		return impl.insert(estado);
	}
	public Estado updateEstado(RequestEntity<Estado> inEntity) {
		Estado estado = getData(inEntity);
		return impl.update(estado);
	}
	public Estado deleteEstado(RequestEntity<Estado> inEntity) {
		Estado estadoID = getData(inEntity);
		Estado estado= impl.findById(estadoID.getEstado_Id()).get();
		impl.deleteEntity(estadoID.getEstado_Id());
		return estado;
	}
	public Estado findById(RequestEntity<Estado> inEntity) {
		return impl.findById(getData(inEntity).getEstado_Id()).get();
	}
	public List<Estado> findAll(){
		return impl.selectAll();
	}
	private Estado getData(RequestEntity<Estado> inEntity) {
		Gson jsonGson = new Gson();
		String json = jsonGson.toJson(inEntity.getBody());
		new Gson();
		return jsonGson.fromJson(json, Estado.class);
	}
}
