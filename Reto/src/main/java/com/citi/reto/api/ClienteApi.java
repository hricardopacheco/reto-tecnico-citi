package com.citi.reto.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citi.reto.controller.ClienteController;
import com.citi.reto.model.Cliente;

@RestController
@RequestMapping(value = "cliente/")
public class ClienteApi {
	 
	 @Autowired
	 private ClienteController controller;
	 
	 
	 @PostMapping("save")
	 public ResponseEntity<Cliente>saveCliente(RequestEntity<Cliente> inEntity) {
		 return new ResponseEntity<Cliente>(controller.saveCliente(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("update")
	 public ResponseEntity<Cliente>updateCliente(RequestEntity<Cliente> inEntity) {
		 return new ResponseEntity<Cliente>(controller.updateCliente(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("delete")
	 public ResponseEntity<Cliente>deleteCliente(RequestEntity<Cliente> inEntity) {
		 return new ResponseEntity<Cliente>(controller.deleteCliente(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("findById")
	 public ResponseEntity<Cliente>findByIdCliente(RequestEntity<Cliente> inEntity) {
		 return new ResponseEntity<Cliente>(controller.findById(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("findAll")
	 public ResponseEntity<List<Cliente>>findAllCliente() {
		 return new ResponseEntity<List<Cliente>>(controller.findAll(),HttpStatus.OK);
	 }
	
}
