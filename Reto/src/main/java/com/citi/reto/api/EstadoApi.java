package com.citi.reto.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citi.reto.controller.EstadoController;
import com.citi.reto.model.Estado;

@RestController
@RequestMapping(value = "estado/")
public class EstadoApi {
	
	 @Autowired
	 private EstadoController controller;
	 
	 @PostMapping("save")
	 public ResponseEntity<Estado> saveEstado(RequestEntity<Estado> inEntity){
		 return new ResponseEntity<Estado>(controller.saveEstado(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("update")
	 public ResponseEntity<Estado> updateEstado(RequestEntity<Estado> inEntity){
		 return new ResponseEntity<Estado>(controller.updateEstado(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("delete")
	 public ResponseEntity<Estado> deleteEstado(RequestEntity<Estado> inEntity){
		 return new ResponseEntity<Estado>(controller.deleteEstado(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("findById")
	 public ResponseEntity<Estado> findByIdEstado(RequestEntity<Estado> inEntity){
		 return new ResponseEntity<Estado>(controller.findById(inEntity),HttpStatus.OK);
	 }
	 
	 @PostMapping("findAll")
	 public ResponseEntity<List<Estado>> findAll(RequestEntity<Estado> inEntity){
		 return new ResponseEntity<List<Estado>>(controller.findAll(),HttpStatus.OK);
	 }
	 

}
