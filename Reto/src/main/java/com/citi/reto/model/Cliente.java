package com.citi.reto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente implements Serializable{
	
	public Cliente(Long cliente_id, String nombre, String ap_mat, String ap_pat, String fechaNacimiento,
			String fechaAlta, boolean status) {
		super();
		this.cliente_id = cliente_id;
		this.nombre = nombre;
		this.ap_mat = ap_mat;
		this.ap_pat = ap_pat;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaAlta = fechaAlta;
		this.status = status;
	}
	
	public Cliente() {}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long cliente_id;
	@Column
	private String nombre;
	@Column
	private String ap_mat;
	@Column
	private String ap_pat;
	@Column
	private String fechaNacimiento;
	@Column
	private String fechaAlta;
	@Column
	private boolean status;
	@Column
	private Long estado_Id;
	
	
	public Long getCliente_id() {
		return cliente_id;
	}
	public void setCliente_id(Long cliente_id) {
		this.cliente_id = cliente_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAp_mat() {
		return ap_mat;
	}
	public void setAp_mat(String ap_mat) {
		this.ap_mat = ap_mat;
	}
	public String getAp_pat() {
		return ap_pat;
	}
	public void setAp_pat(String ap_pat) {
		this.ap_pat = ap_pat;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Long getEstado_Id() {
		return estado_Id;
	}

	public void setEstado_Id(Long estado_Id) {
		this.estado_Id = estado_Id;
	}
	
}
