package com.citi.reto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Estado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long estado_Id;
	@Column
	private String nombre;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "estado_Id")
	private List<Cliente> clientes = new ArrayList<>();
	
	public Long getEstado_Id() {
		return estado_Id;
	}
	public void setEstado_Id(Long estado_Id) {
		this.estado_Id = estado_Id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	
	

}
