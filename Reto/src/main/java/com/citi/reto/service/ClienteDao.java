package com.citi.reto.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.citi.reto.model.Cliente;

@Service
public interface ClienteDao extends JpaRepository<Cliente, Long>{

}
