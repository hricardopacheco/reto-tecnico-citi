package com.citi.reto.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.citi.reto.model.Estado;

@Service
public interface EstadoDao extends JpaRepository<Estado, Long>{

}
