package com.citi.reto.service;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {

	T insert(T entity);
	
	T update(T entity);
	
	List<T> selectAll();
	
	Optional<T> findById(Long id);
	
	void deleteEntity(Long id);
	
	
	
}
