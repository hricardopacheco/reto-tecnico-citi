package com.citi.reto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.citi.reto.model.Cliente;
import com.citi.reto.service.ClienteDao;
import com.citi.reto.service.Dao;


@Repository
@Transactional
public class ClienteImpl implements Dao<Cliente>{

	@Autowired
	private ClienteDao dao;

	@Override
	public Cliente insert(Cliente entity) {
		return dao.save(entity);
	}

	@Override
	public Cliente update(Cliente entity) {
		return dao.save(entity);
	}

	@Override
	public List<Cliente> selectAll() {
		return dao.findAll();
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		return dao.findById(id);
	}

	@Override
	public void deleteEntity(Long id) {
		dao.deleteById(id);
	}
}
