package com.citi.reto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.citi.reto.model.Estado;
import com.citi.reto.service.Dao;
import com.citi.reto.service.EstadoDao;


@Repository
@Transactional
public class EstadoImpl implements Dao<Estado>{
	
	@Autowired
	private EstadoDao dao;

	@Override
	public Estado insert(Estado entity) {
		return dao.save(entity);
	}

	@Override
	public Estado update(Estado entity) {
		return dao.saveAndFlush(entity);
	}

	@Override
	public List<Estado> selectAll() {
		return dao.findAll();
	}

	@Override
	public Optional<Estado> findById(Long id) {
		return dao.findById(id);
	}

	@Override
	public void deleteEntity(Long id) {
		dao.deleteById(id);
		
	}

}
